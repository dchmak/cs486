import Map
import Districts
import House
from pymclevel import alphaMaterials, MCSchematic, MCLevel, BoundingBox
from mcplatform import *
from Utility import *
import random

crops = [59, 141, 142]

def build_farm_cell(cell, map, miny):
    p1 = cell.p1
    p2 = cell.p2

    wall_material = cell.house.district.wall_material
    wall_data = cell.house.district.wall_data
    foundation_material = cell.house.district.foundation_material
    foundation_data = cell.house.district.foundation_data
    fence_material = cell.house.district.fence_material
    fence_data = cell.house.district.fence_data
    roof_material = cell.house.district.roof_material
    roof_data = cell.house.district.roof_data
    pillar_material = cell.house.district.pillar_material
    pillar_data = cell.house.district.pillar_material

    # set planning
    for plan_x in xrange(min((p1.x, p2.x)), max((p1.x, p2.x))):
        for plan_z in xrange(min((p1.z, p2.z)), max((p1.z, p2.z))):
            map.plan[plan_x - map.origin.x][plan_z - map.origin.z] = Map.farm
            map.height[plan_x - map.origin.x][plan_z - map.origin.z] = p1.y - map.origin.y

    # fill with hydrated farmland
    fill(p1, cell.size.x, 1, cell.size.z, map.level, 60, 7)

    # used to randomize water a little bit
    r = random.randint(0, 3)

    # plant crops / water
    for i in range(cell.size.x - 2):
        for j in range(cell.size.z - 2):
            x = p1.x + 1 + i
            z = p1.z + 1 + j
            if x + r % 3 == 0 and z + r % 3 == 0:
                map.level.setBlockAt(x, p1.y, z, 9) # water
            else:
                material = crops[cell.house.plant_seed]
                data = random.randint(0, 7)
                if data < 4: # more likely to make bigger crops
                    data = random.randint(0, 7)
                map.level.setBlockAt(x, p1.y + 1, z, material)
                map.level.setBlockDataAt(x, p1.y + 1, z, data)


    #build walls
    p = point3d(p1.x, p1.y, p1.z)
    fill(p, 1, 1, cell.size.z, map.level, pillar_material, pillar_data)

    p = point3d(p2.x - 1, p1.y, p1.z)
    fill(p, 1, 1, cell.size.z, map.level, pillar_material, pillar_data)

    p = point3d(p1.x, p1.y, p1.z)
    fill(p, cell.size.x, 1, 1, map.level, pillar_material, pillar_data)

    p = point3d(p1.x, p1.y, p2.z - 1)
    fill(p, cell.size.x, 1, 1, map.level, pillar_material, pillar_data)

    if Districts.debug:
        map.level.setBlockAt       (p1.x, p1.y, p1.z, 20)
        map.level.setBlockDataAt   (p1.x, p1.y, p1.z, 0)