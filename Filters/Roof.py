import House
import Map
from Cells import *
from Utility import *
from pymclevel import alphaMaterials, MCSchematic, MCLevel, BoundingBox
from mcplatform import *


def build_roof(cell, map, roof_material, roof_data, floor_material, floor_data, wall_material, wall_data):
    level = map.level
    p1 = cell.p1
    p2 = cell.p2

    x_minus_free = (cell.neighbours[Map.x_minus] is None) or (cell.neighbours[Map.x_minus] is not None and cell.neighbours[Map.x_minus].neighbours[Map.y_plus] is not None)
    x_plus_free = (cell.neighbours[Map.x_plus] is None) or (cell.neighbours[Map.x_plus] is not None and cell.neighbours[Map.x_plus].neighbours[Map.y_plus] is not None)
    z_minus_free = (cell.neighbours[Map.z_minus] is None) or (cell.neighbours[Map.z_minus] is not None and cell.neighbours[Map.z_minus].neighbours[Map.y_plus] is not None)
    z_plus_free = (cell.neighbours[Map.z_plus] is None) or (cell.neighbours[Map.z_plus] is not None and cell.neighbours[Map.z_plus].neighbours[Map.z_plus] is not None)
    
    repeat = min(cell.size.x, cell.size.z)

    #FILL CEILING
    fill(point3d(p1.x + 2, p2.y - 1, p1.z + 2), cell.size.x - 4, 1, cell.size.z - 4, level, floor_material, floor_data)

    #FILL BEAMS
    if cell.neighbours[Map.x_plus] is not None and cell.neighbours[Map.x_plus].type == building:
        fill(point3d(p2.x - 2, p2.y - 1, p1.z + 2), 3, 1, cell.size.z - 4, level, floor_material, floor_data)
    if cell.neighbours[Map.x_minus] is not None and cell.neighbours[Map.x_minus].type == building:
        fill(point3d(p1.x - 1, p2.y - 1, p1.z + 2), 3, 1, cell.size.z - 4, level, floor_material, floor_data)
    if cell.neighbours[Map.z_plus] is not None and cell.neighbours[Map.z_plus].type == building:
        fill(point3d(p1.x + 2, p2.y - 1, p2.z - 2), cell.size.x - 4, 1, 3, level, floor_material, floor_data)
    if cell.neighbours[Map.z_minus] is not None and cell.neighbours[Map.z_minus].type == building:
        fill(point3d(p1.x + 2, p2.y - 1, p1.z - 1), cell.size.x - 4, 1, 3, level, floor_material, floor_data)

    #CROSS
    if x_minus_free and x_plus_free and z_minus_free and z_plus_free:
        # roof
        for i in range(repeat):
            height = p2.y + (repeat // 2) - abs(repeat // 2 - i)
            fill(point3d(p1.x + i, height, p1.z), 1, 1, cell.size.z, level,
                 roof_material, roof_data)
            fill(point3d(p1.x, height, p1.z + i), cell.size.x, 1, 1, level,
                 roof_material, roof_data)
                 
            # roof hole wall
            fill_x_minus_hole(p1, i, height, cell, level, wall_material, wall_data)
            fill_x_plus_hole(p1, i, height, cell, level, wall_material, wall_data)
            fill_z_minus_hole(p1, i, height, cell, level, wall_material, wall_data)
            fill_z_plus_hole(p1, i, height, cell, level, wall_material, wall_data)
        
    #LINES
    elif x_minus_free and x_plus_free:
        # roof
        for i in range(repeat):
            height = p2.y + (repeat // 2) - abs(repeat // 2 - i)
            fill(point3d(p1.x + i, height, p1.z), 1, 1, cell.size.z, level,
                 roof_material, roof_data)

            # roof hole wall
            fill_z_minus_hole(p1, i, height, cell, level, wall_material, wall_data)
            fill_z_plus_hole(p1, i, height, cell, level, wall_material, wall_data)
    elif z_minus_free and z_plus_free:
        # roof
        for i in range(repeat):
            height = p2.y + (repeat // 2) - abs(repeat // 2 - i)
            fill(point3d(p1.x, height, p1.z + i), cell.size.x, 1, 1, level,
                 roof_material, roof_data)

            # roof hole wall
            fill_x_minus_hole(p1, i, height, cell, level, wall_material, wall_data)
            fill_x_plus_hole(p1, i, height, cell, level, wall_material, wall_data)

    #CORNERS
    elif z_minus_free and x_minus_free:
        # roof
        for i in range(repeat):
            height = p2.y + (repeat // 2) - abs(repeat // 2 - i)
            fill(point3d(p1.x + i, height, p1.z + i), 1, 1, cell.size.z - i, level, roof_material, roof_data)
            fill(point3d(p1.x + i, height, p1.z + i), cell.size.x - i, 1, 1, level, roof_material, roof_data)

            # roof hole wall
            fill_x_plus_hole(p1, i, height, cell, level, wall_material, wall_data)
            fill_z_plus_hole(p1, i, height, cell, level, wall_material, wall_data)
    elif z_minus_free and x_plus_free:
        # roof
        for i in range(repeat):
            height = p2.y + (repeat // 2) - abs(repeat // 2 - i)
            fill(point3d(p1.x + cell.size.x - i - 1, height, p1.z + i), 1, 1, cell.size.z - i,
                 level, roof_material, roof_data)
            fill(point3d(p1.x, height, p1.z + i), cell.size.x - i, 1, 1,
                 level, roof_material, roof_data)

            # roof hole wall
            fill_x_minus_hole(p1, i, height, cell, level, wall_material, wall_data)
            fill_z_plus_hole(p1, i, height, cell, level, wall_material, wall_data)
    elif z_plus_free and x_minus_free:
        # roof
        for i in range(repeat):
            height = p2.y + (repeat // 2) - abs(repeat // 2 - i)
            fill(point3d(p1.x + i, height, p1.z), 1, 1, cell.size.z - i, level, roof_material, roof_data)
            fill(point3d(p1.x + i, height, p1.z + cell.size.z - i - 1), cell.size.x - i, 1, 1,
                 level, roof_material, roof_data)

            # roof hole wall
            fill_z_minus_hole(p1, i, height, cell, level, wall_material, wall_data)
            fill_x_plus_hole(p1, i, height, cell, level, wall_material, wall_data)
    elif z_plus_free and x_plus_free:
        # roof
        for i in range(repeat):
            height = p2.y + (repeat // 2) - abs(repeat // 2 - i)
            fill(point3d(p1.x + cell.size.x - i - 1, height, p1.z), 1, 1, cell.size.z - i,
                 level, roof_material, roof_data)
            fill(point3d(p1.x, height, p1.z + cell.size.z - i - 1), cell.size.x - i, 1, 1,
                 level, roof_material, roof_data)
                 
            # roof hole wall
            fill_z_minus_hole(p1, i, height, cell, level, wall_material, wall_data)
            fill_x_minus_hole(p1, i, height, cell, level, wall_material, wall_data)

    #T-CROSSINGS
    elif z_minus_free:
        # roof
        for i in range(repeat):
            height = p2.y + (repeat // 2) - abs(repeat // 2 - i)
            fill(point3d(p1.x, height, p1.z + i), cell.size.x, 1, 1, level,
                 roof_material, roof_data)
            fill(point3d(p1.x + i, height, p1.z + cell.size.z - cell.size.z // 2), 1, 1, cell.size.z // 2, level,
                 roof_material, roof_data)

            # roof hole wall
            fill_x_minus_hole(p1, i, height, cell, level, wall_material, wall_data)
            fill_x_plus_hole(p1, i, height, cell, level, wall_material, wall_data)
            fill_z_plus_hole(p1, i, height, cell, level, wall_material, wall_data)
    elif z_plus_free:
        # roof
        for i in range(repeat):
            height = p2.y + (repeat // 2) - abs(repeat // 2 - i)
            fill(point3d(p1.x, height, p1.z + i), cell.size.x, 1, 1, level,
                 roof_material, roof_data)
            fill(point3d(p1.x + i, height, p1.z), 1, 1, cell.size.z // 2, level,
                 roof_material, roof_data)

            # roof hole wall
            fill_x_minus_hole(p1, i, height, cell, level, wall_material, wall_data)
            fill_x_plus_hole(p1, i, height, cell, level, wall_material, wall_data)
            fill_z_minus_hole(p1, i, height, cell, level, wall_material, wall_data)
    elif x_minus_free:
        # roof
        for i in range(repeat):
            height = p2.y + (repeat // 2) - abs(repeat // 2 - i)
            fill(point3d(p1.x + i, height, p1.z), 1, 1, cell.size.z, level,
                 roof_material, roof_data)
            fill(point3d(p1.x + cell.size.x - cell.size.x // 2, height, p1.z + i), cell.size.x // 2, 1, 1, level,
                 roof_material, roof_data)

            # roof hole wall
            fill_x_plus_hole(p1, i, height, cell, level, wall_material, wall_data)
            fill_z_minus_hole(p1, i, height, cell, level, wall_material, wall_data)
            fill_z_plus_hole(p1, i, height, cell, level, wall_material, wall_data)
    elif x_plus_free:
        # roof
        for i in range(repeat):
            height = p2.y + (repeat // 2) - abs(repeat // 2 - i)
            fill(point3d(p1.x + i, height, p1.z), 1, 1, cell.size.z, level,
                 roof_material, roof_data)
            fill(point3d(p1.x, height, p1.z + i), cell.size.x // 2, 1, 1, level,
                 roof_material, roof_data)

            # roof hole wall
            fill_x_minus_hole(p1, i, height, cell, level, wall_material, wall_data)
            fill_z_minus_hole(p1, i, height, cell, level, wall_material, wall_data)
            fill_z_plus_hole(p1, i, height, cell, level, wall_material, wall_data)

    #CROSS
    else:
        # roof
        for i in range(repeat):
            height = p2.y + (repeat // 2) - abs(repeat // 2 - i)
            fill(point3d(p1.x + i, height, p1.z), 1, 1, cell.size.z, level,
                 roof_material, roof_data)
            fill(point3d(p1.x, height, p1.z + i), cell.size.x, 1, 1, level,
                 roof_material, roof_data)

            # roof hole wall
            fill_x_minus_hole(p1, i, height, cell, level, wall_material, wall_data)
            fill_x_plus_hole(p1, i, height, cell, level, wall_material, wall_data)
            fill_z_minus_hole(p1, i, height, cell, level, wall_material, wall_data)
            fill_z_plus_hole(p1, i, height, cell, level, wall_material, wall_data)


def fill_z_plus_hole(p1, i, height, cell, level, wall_material, wall_data):
    p = point3d(p1.x + i + 1, height, p1.z + cell.size.z - 2)
    fill(p, cell.size.x - 2 * (i + 1), 1, 1, level, wall_material, wall_data)


def fill_z_minus_hole(p1, i, height, cell, level, wall_material, wall_data):
    p = point3d(p1.x + i + 1, height, p1.z + 1)
    fill(p, cell.size.x - 2 * (i + 1), 1, 1, level, wall_material, wall_data)

        
def fill_x_plus_hole(p1, i, height, cell, level, wall_material, wall_data):
    p = point3d(p1.x + cell.size.x - 2, height, p1.z + i + 1)
    fill(p, 1, 1, cell.size.z - 2 * (i + 1), level, wall_material, wall_data)

        
def fill_x_minus_hole(p1, i, height, cell, level, wall_material, wall_data):
    p = point3d(p1.x + 1, height, p1.z + i + 1)
    fill(p, 1, 1, cell.size.z - 2 * (i + 1), level, wall_material, wall_data)
