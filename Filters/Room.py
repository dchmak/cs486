from pymclevel import alphaMaterials, MCSchematic, MCLevel, BoundingBox
from mcplatform import *

import Cells
import Furniture
import Districts
import Map
import Utility
import random


max_attempt = 5


class Room:
    def __init__(self, house, level):
        self.house = house
        self.level = level


    def assign_cells(self, cell_list, debug_data=0):
        self.cell_list = cell_list

        if Districts.debug:
            for cell in cell_list:
                self.level.setBlockAt       ((cell.p1.x + cell.p2.x) / 2, cell.p1.y, (cell.p1.z + cell.p2.z) / 2, 35)
                self.level.setBlockDataAt   ((cell.p1.x + cell.p2.x) / 2, cell.p1.y, (cell.p1.z + cell.p2.z) / 2, debug_data)

    def find_location(self, furniture, force=False, orient_override=None):
        attempt = 0
        cells = list(self.cell_list)
        while len(cells) > 0 and (force or attempt < max_attempt):
            cell = random.choice(cells)

            orient = random.choice(Map.flat_dirs if orient_override is None else orient_override)
            x = random.randint(cell.p1.x + 2, cell.p2.x - 2 - furniture.get_bounding_box_size(orient).x)
            y = cell.p1.y + 1
            z = random.randint(cell.p1.z + 2, cell.p2.z - 2 - furniture.get_bounding_box_size(orient).z)

            xminus_check = cell.neighbours[Map.x_minus] is None
            xplus_check = cell.neighbours[Map.x_plus] is None
            zminus_check = cell.neighbours[Map.z_minus] is None
            zplus_check = cell.neighbours[Map.z_plus] is None
            if furniture.is_corner_furniture():
                choices = []
                if orient_override is None or (Map.x_minus in orient_override and Map.z_minus in orient_override):
                    if xminus_check and zminus_check:
                        o = random.choice([Map.x_minus, Map.z_minus])
                        choices.append((cell.p1.x + 2, cell.p1.z + 2, o))
                        
                if orient_override is None or (Map.x_minus in orient_override and Map.z_plus in orient_override):
                    if xminus_check and zplus_check:
                        o = random.choice([Map.x_minus, Map.z_plus])
                        choices.append((cell.p1.x + 2, cell.p2.z - 2 - furniture.get_bounding_box_size(o).z, o))
                        
                if orient_override is None or (Map.x_plus in orient_override and Map.z_minus in orient_override):
                    if xplus_check and zminus_check:
                        o = random.choice([Map.x_plus, Map.z_minus])
                        choices.append((cell.p2.x - 2 - furniture.get_bounding_box_size(o).x, cell.p1.z + 2, o))
                        
                if orient_override is None or (Map.x_plus in orient_override and Map.z_plus in orient_override):
                    if xplus_check and zplus_check:
                        o = random.choice([Map.x_plus, Map.z_plus])
                        choices.append((cell.p2.x - 2 - furniture.get_bounding_box_size(o).x, cell.p2.z - 2 - furniture.get_bounding_box_size(o).z, o))

                if len(choices) <= 0:
                    cells.remove(cell)
                    continue

                x, z, orient = random.choice(choices)
            elif furniture.is_wall_furniture():
                choices = []
                
                if orient_override is None or Map.x_minus in orient_override:
                    if xminus_check:
                        choices.append((cell.p1.x + 2, z, Map.x_minus))
                        
                if orient_override is None or Map.x_plus in orient_override:
                    if xplus_check:
                        choices.append((cell.p2.x - 2 - furniture.get_bounding_box_size(Map.x_plus).x, z, Map.x_plus))
                        
                if orient_override is None or Map.z_minus in orient_override:
                    if zminus_check:
                        choices.append((x, cell.p1.z + 2, Map.z_minus))
                        
                if orient_override is None or Map.z_plus in orient_override:
                    if zplus_check:
                        choices.append((x, cell.p2.z - 2 - furniture.get_bounding_box_size(Map.z_plus).z, Map.z_plus))

                if len(choices) <= 0:
                    cells.remove(cell)
                    continue
                
                x, z, orient = random.choice(choices)

            if furniture.is_ceiling_furniture():
                y = cell.p2.y - 2 - furniture.get_bounding_box_size(orient).y

            p = Utility.point3d(x, y, z)
            
            if Utility.is_full(
                    Utility.point3d(x, y-1, z), 
                    furniture.get_bounding_box_size(orient).x, 
                    1, 
                    furniture.get_bounding_box_size(orient).z,
                    self.level):
                if force or Utility.is_empty(
                        p, 
                        furniture.get_bounding_box_size(orient).x, 
                        furniture.get_bounding_box_size(orient).y, 
                        furniture.get_bounding_box_size(orient).z,
                        self.level):
                    return p, orient

            attempt += 1

        if force:
            cell = random.choice(self.cell_list)
            orient = random.choice(Map.flat_dirs if orient_override is None else orient_override)
            x = random.randint(cell.p1.x + 2, cell.p2.x - 2 - furniture.get_bounding_box_size(orient).x)
            y = cell.p1.y + 1
            z = random.randint(cell.p1.z + 2, cell.p2.z - 2 - furniture.get_bounding_box_size(orient).z)            
            
            if furniture.is_ceiling_furniture():
                y = cell.p2.y - 2 - furniture.get_bounding_box_size(orient).y

            p = Utility.point3d(x, y, z)
            
            if Utility.is_full(
                    Utility.point3d(x, y-1, z), 
                    furniture.get_bounding_box_size(orient).x, 
                    1, 
                    furniture.get_bounding_box_size(orient).z,
                    self.level):
                return p, orient

        return None, None


    def populate(self, force=False):
        # mandatory furnitures
        for furniture in self.get_mandatory_furnitures():
            # select a location
            location, orient = self.find_location(furniture, force)

            # no space to add more
            if location is None:
                break

            furniture.build(location, orient)

        # Chance to add optional furnitures
        while len(self.get_optional_furnitures()) > 0:
            # select a furniture
            furniture = random.choice(self.get_optional_furnitures())

            # select a location
            location, orient = self.find_location(furniture)

            # no space to add more
            if location is None:
                break

            # execute build instructions
            furniture.build(location, orient)


    def get_mandatory_furnitures(self):
        return []


    def get_optional_furnitures(self):
        return []


    def get_multiple_cells_probability(self):
        return 0


class Bedroom(Room):
    def get_mandatory_furnitures(self):
        return [Furniture.SmallBed(self.level)]


    def get_multiple_cells_probability(self):
        return 0.5


class Kitchen(Room):
    def get_mandatory_furnitures(self):
        return [Furniture.Fridge(self.level)]


    def get_multiple_cells_probability(self):
        return 0.8


class StairRoom(Room):
    def __init__(self, house, level, height):
        Room.__init__(self, house, level)
        self.height = height


    def get_mandatory_furnitures(self):
        return [Furniture.Stair(self.level, self.height)]
        

class DoorRoom(Room):
    def __init__(self, house, level, preferred_orient):
        Room.__init__(self, house, level)
        self.preferred_orient = preferred_orient


    def find_location(self, furniture, force=False):
        p, self.door_orientation = Room.find_location(self, furniture, force, self.preferred_orient)

        if p is None:
            self.door_coordinates = None
        elif self.door_orientation is Map.z_plus:
            self.door_coordinates = Utility.point3d(p.x + 1, p.y, p.z + 3)
        elif self.door_orientation is Map.x_minus:
            self.door_coordinates = Utility.point3d(p.x - 1, p.y, p.z + 1)
        elif self.door_orientation is Map.z_minus:
            self.door_coordinates = Utility.point3d(p.x + 1, p.y, p.z - 1)
        else:
            self.door_coordinates = Utility.point3d(p.x + 3, p.y, p.z + 1)

        return p, self.door_orientation


    def get_mandatory_furnitures(self):
        return [Furniture.Door(self.level)]
