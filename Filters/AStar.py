import Map
import heapq
from pymclevel import alphaMaterials, MCSchematic, MCLevel, BoundingBox
from mcplatform import *
from Utility import point2d
from Utility import distance2d

def find_road_path(origin, map):
    print("FINDING PATH TO ROAD from (%i, %i)" % (origin.x, origin.z))
    district_point = map.district[origin.x][origin.z].origin
    is_goal = lambda state: map.plan[state % map.w][state // map.w] == Map.road
    heuristic = lambda state: distance2d(map.int_to_point2d(state), point2d(district_point.x, district_point.z), 2)
    def cost(path):
        sum = 0
        for i in range(len(path) - 2):
            point = map.int_to_point2d(path[i])
            next_next = map.int_to_point2d(path[i + 2])
            sum += pow(pow(point.x - next_next.x, 2) + pow(point.z - next_next.z, 2), 0.5)
            sum += map.roughness[point.x][point.z] # add roughness as cost
        return sum

    def get_successors(state):
        successors = []

        x = state % map.w
        z = state // map.w
        x_plus_border = (x == map.w - 1) or (map.x_roughness[x][z]) or (map.water[x + 1][z])
        x_minus_border = (x == 0) or (map.x_roughness[x - 1][z]) or (map.water[x - 1][z])
        z_plus_border = (z == map.d - 1) or (map.z_roughness[x][z]) or (map.water[x][z + 1])
        z_minus_border = (z == 0) or (map.z_roughness[x][z - 1]) or (map.water[x][z - 1])

        if not x_plus_border and map.plan[x + 1][z] in [Map.empty, Map.road, Map.farm]:
            successors.append((x + 1) + map.w * (z))
        if not x_minus_border and map.plan[x - 1][z] in [Map.empty, Map.road, Map.farm]:
            successors.append((x - 1) + map.w * (z))
        if not z_plus_border and map.plan[x][z + 1] in [Map.empty, Map.road, Map.farm]:
            successors.append((x) + map.w * (z + 1))
        if not z_minus_border and map.plan[x][z - 1] in [Map.empty, Map.road, Map.farm]:
            successors.append((x) + map.w * (z - 1))

        return successors

    path = a_star(map.point2d_to_int(origin), get_successors, is_goal, cost, heuristic)

    if path is None:
        return path

    # Convert states from integers to 2d points
    point_path = []
    for int in path:
        point_path.append(map.int_to_point2d(int))

    return point_path

def get_point_path(origin, goal, map):
    print("FINDING PATH from (%i, %i) to (%i, %i)" % (origin.x, origin.z, goal.x, goal.z))
    is_goal = lambda state : state % map.w == goal.x and state // map.w == goal.z
    heuristic = lambda state : distance2d(map.int_to_point2d(state), goal, 2)
    def cost(path):
        sum = 0
        for i in range(len(path) - 2):
            point = map.int_to_point2d(path[i])
            next_next = map.int_to_point2d(path[i + 2])
            sum += pow(pow(point.x - next_next.x, 2) + pow(point.z - next_next.z, 2), 0.5)
            sum += map.roughness[point.x][point.z] # add roughness as cost
        return sum

    def get_successors(state):
        successors = []

        x = state % map.w
        z = state // map.w
        x_plus_border = (x == map.w - 1) or (map.x_roughness[x][z]) or (map.water[x + 1][z])
        x_minus_border = (x == 0) or (map.x_roughness[x - 1][z]) or (map.water[x - 1][z])
        z_plus_border = (z == map.d - 1) or (map.z_roughness[x][z]) or (map.water[x][z + 1])
        z_minus_border = (z == 0) or (map.z_roughness[x][z - 1]) or (map.water[x][z - 1])

        if not x_plus_border and map.plan[x + 1][z] in [Map.empty, Map.road]:
            successors.append((x + 1) + map.w * (z))
        if not x_minus_border and map.plan[x - 1][z] in [Map.empty, Map.road]:
            successors.append((x - 1) + map.w * (z))
        if not z_plus_border and map.plan[x][z + 1] in [Map.empty, Map.road]:
            successors.append((x) + map.w * (z + 1))
        if not z_minus_border and map.plan[x][z - 1] in [Map.empty, Map.road]:
            successors.append((x) + map.w * (z - 1))

        return successors

    path = a_star(map.point2d_to_int(origin), get_successors, is_goal, cost, heuristic)

    if path is None:
        return path

    # Convert states from integers to 2d points
    point_path = []
    for int in path:
        point_path.append(map.int_to_point2d(int))

    return point_path

def a_star(start_state, get_successors, is_goal, cost, heuristic):
    visited = set()
    frontier = []
    frontier_size = 0
    current_state = start_state
    expansion = 0
    path = [current_state]

    while not is_goal(current_state):

        expansion += 1

        for successor in get_successors(current_state):
            new_path = list(path)
            new_path.append(successor)
            tuple = (cost(new_path) + heuristic(new_path[-1]), new_path)
            heapq.heappush(frontier, tuple)
            frontier_size += 1

        visited.add(current_state)
        if(len(frontier) == 0):
            return None
        path = heapq.heappop(frontier)[1]
        frontier_size -= 1
        current_state = path[-1]

        while current_state in visited:  # prune
            if frontier_size == 0:
                print("!Pathfinding FAILED!")
                return None
            item = heapq.heappop(frontier)
            frontier_size -= 1
            path = item[1]
            current_state = path[-1]

        # Give up
        if expansion > 100000:
            print("!Pathfinding FAILED!")
            return None

    print("Cost of the solution: " + str(cost(path)))
    print("Number of states expanded: " + str(expansion))

    return path