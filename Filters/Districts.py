import Map
import House
from pymclevel import alphaMaterials, MCSchematic, MCLevel, BoundingBox
from mcplatform import *
from random import Random
from collections import namedtuple
from collections import deque
from Utility import distance3d
from Utility import point3d
from Utility import point2d
from Roads import add_roads
from Lamps import lamp_it_up

max_districts = 20
district_num_input = "Number of target districts"
district_gen_input = "Initial number of districts generated"
distance_power_input = "Distance power"
expand_diagonal_input = "Expand diagonally"
debug_input = 'Debug'
debug = False

# When a point is sufficiently far away from a district, it may be overtaken by a closer district.
# This is the difference in distance required for this to happen
distance_overpowering_diffrencial = 3

Claim = namedtuple("Claim", ["x", "z", "district"])

inputs = (
    (district_num_input, 5),
    (district_gen_input, 10),
    (distance_power_input, 1.5),
    (expand_diagonal_input, False),
    (debug_input, False),
)

# DISTRICT TYPES
housing = 0
farming = 1
types = {0: "housing",
         1: "farming"}

district_type_order = [0, 1, 0, 0, 1, 0, 1, 0, 0, 1]

class district:
    def __init__(self, origin, num):
        self.type = 0
        self.name = str(num)
        self.origin = origin
        self.size = 0
        self.water = 0
        self.roughness = 0
        self.adjacency = {}
        self.setPallette()

    def amount_water(self):
        return float(self.water) / self.size

    def average_roughness(self):
        return float(roughness) / self.size

    def setPallette(self):
        self.foundation_material = 4
        self.foundation_data = 0

        self.wall_material = 4
        self.wall_data = 0

        self.roof_material = 4
        self.roof_data = 0

        self.pillar_material = 4
        self.pillar_data = 0

        self.fence_material = 85
        self.fence_data = 0

def perform(level, box, options):
    global debug

    print("--------")
    print("STARTING FILTER  ")
    map = Map.Map(level, box)
    print("Box: (%i, %i, %i) to (%i, %i, %i)" % (box.minx, box.miny, box.minz, box.maxx, box.maxy, box.maxz))
    print("Map: w=" + str(map.w) + ", h=" + str(map.h) + ", d=" + str(map.d))
    goal_num = min(options[district_num_input], max_districts)
    district_num = goal_num
    district_gen = min(options[district_gen_input], max_districts)
    distance_power = options[distance_power_input]
    expand_diagonal = options[expand_diagonal_input]
    debug = options[debug_input]

    print("Generating " + str(district_gen) + " districts with the goal of " + str(district_num))
    
    districts = []

    points = generate_points(district_gen, map)

    flood_map(map, districts, map.district, points, distance_power, expand_diagonal)

    #initial survey
    survey(map, districts, map.district)

    #Fill empty points
    for x in range(map.w):
        for z in range(map.d):
            if map.district[x][z] == None:
                flood_map(map, districts, map.district, [point3d(x, map.height[x][z], z)], distance_power, expand_diagonal)

    #final survey
    survey(map, districts, map.district)

    #reduce map
    reduce(map, districts, map.district, district_num)

    #find midpoints
    find_origin(map, districts, map.district)

    map.districts = districts
    House.load_palette(map, box)

    #Wool markings
    if debug:
        add_wool(map, districts, map.district)

    #Roads
    add_roads(districts, map)

    #Mark centers
    if debug:
        for district in districts:
            level.setBlockAt(map.origin.x + district.origin.x, map.origin.y + district.origin.y + 2, map.origin.z + district.origin.z, 57)

    #Generate Types
    generate_types(districts)

    #Generate Buildings
    generate_buildings(map)

    #Lamp it up!
    lamp_it_up(map)

    #Stats
    for district in districts:
        s = "DISTRICT %s \n" \
            "  Origin: (%i, %i, %i) \n" \
            "  Type: %s\n" \
            "  Size: %i\n" \
            "  %%Water: %f\n" \
            "  Adjacency: [" % \
            (district.name,
             district.origin.x, district.origin.y, district.origin.z,
             types[district.type],
             district.size,
             float(district.water) * 100 / district.size)
        for other_district, amount in district.adjacency.items():
            s += ("%s: %i, " % (other_district.name, amount))
        s += ("]")
        print(s)

def generate_types(districts):
    count = 0
    for district in districts:
        if count < len(district_type_order):
            district.type = district_type_order[count]
        count += 1

def generate_buildings(map):
    for i in range(map.w // 8):
        for j in range(map.d // 8):
            random = Random()
            x = i * 16 - 3 + random.randrange(8)
            z = j * 16 - 3 + random.randrange(8)
            House.build_house(x, z, map)

            

def generate_points(district_num, map):
    points = []
    for i in range(district_num):
        random = Random()
        x = random.randrange(map.w)
        z = random.randrange(map.d)
        point = point3d(x, map.height[x][z], z)
        points.append(point)
    return points

# uses flood fill to determine districts
def flood_map(map, districts, district_map, points, distance_power, expand_diagonal):
    claim_queue = deque()

    # enqueue district claim for each point
    for i in range(len(points)):
        point = points[i]
        new_district = district(point, len(districts))
        districts.append(new_district)
        claim = Claim(point.x, point.z, new_district)
        claim_queue.append(claim)

    while len(claim_queue) > 0:
        claim = claim_queue.popleft()

        current_district = district_map[claim.x][claim.z]

        # Skip claim if it is already in district
        if current_district is claim.district:
            continue

        # If tile is claimed already, check if distance is sufficient to override. Else skip this claim.
        if not current_district is None:

            # Measures distance between district origins and this point
            this_point = point3d(claim.x, map.height[claim.x][claim.z], claim.z)
            current_distance = distance3d(this_point, current_district.origin, distance_power)
            new_distance = distance3d(this_point, claim.district.origin, distance_power)

            # Checks if new distance is sufficiently closer
            if current_distance - new_distance < distance_overpowering_diffrencial:
                continue
            else:
                current_district.size -= 1 # the previous district shrunk by that tile

        # CLAIMING THE TILE
        district_map[claim.x][claim.z] = claim.district
        claim.district.size += 1 # the claim's district grew

        x_plus_border = (claim.x == map.w - 1) or (map.x_roughness[claim.x][claim.z])
        x_minus_border = (claim.x == 0) or (map.x_roughness[claim.x - 1][claim.z])
        z_plus_border = (claim.z == map.d - 1) or (map.z_roughness[claim.x][claim.z])
        z_minus_border = (claim.z == 0) or (map.z_roughness[claim.x][claim.z - 1])

        #expand for each empty border
        if not x_plus_border:
            claim_queue.append(Claim(claim.x + 1, claim.z, claim.district))
        if not x_minus_border:
            claim_queue.append(Claim(claim.x - 1, claim.z, claim.district))
        if not z_plus_border:
            claim_queue.append(Claim(claim.x, claim.z + 1, claim.district))
        if not z_minus_border:
            claim_queue.append(Claim(claim.x, claim.z - 1, claim.district))

        #expand diagonally
        if expand_diagonal and not x_plus_border and not z_plus_border:
            claim_queue.append(Claim(claim.x + 1, claim.z + 1, claim.district))
        if expand_diagonal and not x_minus_border and not z_plus_border:
            claim_queue.append(Claim(claim.x - 1, claim.z + 1, claim.district))
        if expand_diagonal and not x_plus_border and not z_minus_border:
            claim_queue.append(Claim(claim.x + 1, claim.z - 1, claim.district))
        if expand_diagonal and not x_minus_border and not z_minus_border:
            claim_queue.append(Claim(claim.x - 1, claim.z - 1, claim.district))

# Finds average point in each district
def find_origin(map, districts, district_map):

    x_totals = dict()
    z_totals = dict()
    sizes = dict()

    for district in districts:
        x_totals[district] = 0
        z_totals[district] = 0
        sizes[district] = 0

    for x in range(map.w):
        for z in range(map.d):
            district = district_map[x][z]

            #skip empties
            if district is None:
                continue

            sizes[district] += 1
            x_totals[district] += x
            z_totals[district] += z

    for district in districts:
        district.size = sizes[district]
        district.origin.x = x_totals[district] // district.size
        district.origin.z = z_totals[district] // district.size
        district.origin.y = map.height[district.origin.x][district.origin.z]


def survey(map, districts, district_map):
    for district in districts:
        district.water = 0
        district.roughness = 0
        district.adjacency = {}

    for x in range(map.w):
        for z in range(map.d):
            district = district_map[x][z]

            #skip empties
            if district is None:
                continue

            #tally water
            if map.water[x][z]:
                district.water += 1

            #tally roughness
            district.roughness += map.roughness[x][z]

            #tally adjacency
            adjacent_districts = []
            if x > 0 and not district_map[x - 1][z] is None:
                adjacent_districts.append(district_map[x - 1][z])
            if x < map.w - 1 and not district_map[x + 1][z] is None:
                adjacent_districts.append(district_map[x + 1][z])
            if z > 0 and not district_map[x][z - 1] is None:
                adjacent_districts.append(district_map[x][z - 1])
            if z < map.d - 1 and not district_map[x][z + 1] is None:
                adjacent_districts.append(district_map[x][z + 1])
            for other_district in adjacent_districts:
                if not district is other_district:
                    if other_district in district.adjacency.keys():
                        district.adjacency[other_district] += 1
                        other_district.adjacency[district] += 1
                    else:
                        district.adjacency[other_district] = 1
                        other_district.adjacency[district] = 1


def add_wool(map, districts, district_map):
    for x in range(map.w):
        for z in range(map.d):
            district = district_map[x][z]
            if not district is None:
                real_x = x + map.origin.x
                real_y = map.height[x][z] + map.origin.y
                real_z = z + map.origin.z
                map.level.setBlockAt(real_x, real_y, real_z, 35)
                map.level.setBlockDataAt(real_x, real_y, real_z, districts.index(district))

def merge(map, district_map, old_district, new_district):
    #Copy data
    new_district.roughness += old_district.roughness
    new_district.water += old_district.water
    for district in old_district.adjacency.keys():
        #  A district can't be adjacent to itself
        if district is new_district:
            continue
        #  Add other adjacencies
        if district in new_district.adjacency.keys():
            new_district.adjacency[district] += old_district.adjacency[district]
        else:
            new_district.adjacency[district] = old_district.adjacency[district]

    #Merge districts
    for x in range(map.w):
        for z in range(map.d):
            if district_map[x][z] == old_district:
                district_map[x][z] = new_district

def merge_or_cull(map, district, districts, district_map):
    if len(district.adjacency.keys()) is 0:
        print("Culling district " + district.name)
        #cull
        for x in range(map.w):
            for z in range(map.d):
                if district_map[x][z] is district:
                    district_map[x][z] = None
    else:
        #merge
        new_district = district.adjacency.keys()[0]
        print("Merging district " + district.name + " into " + new_district.name)
        for candidate, adjacency in district.adjacency.items():
            if float(district.adjacency[new_district]) / new_district.size \
                    < float(district.adjacency[candidate]) / candidate.size:
                new_district = candidate
        merge(map, district_map, district, new_district)

    #  Remove adjacencies
    for other in districts:
        if district in other.adjacency.keys():
            other.adjacency.pop(district)

    districts.remove(district)


def reduce(map, districts, district_map, district_num):
    print("Reducing districts:")
    min_district_size = (map.w * map.d) / (district_num * 5)

    reduce_list = []
    for district in districts:
        if district.size < min_district_size:
            reduce_list.append(district)

    for district in reduce_list:
        merge_or_cull(map, district, districts, district_map)

    while len(districts) > district_num:
        smallest = districts[0]
        for district in districts:
            if smallest.size > district.size:
                smallest = district
        merge_or_cull(map, smallest, districts, district_map)

    for x in range(map.w):
        for z in range(map.d):
            if district_map[x][z] not in districts:
                district_map[x][z] = None
