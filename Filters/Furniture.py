import Districts
import Map
import Utility


class Furniture():
    def __init__(self, level):
        self.level = level


    # returns (w, h, d)
    def get_bounding_box_size(self, orient):
        return Utility.point3d(0, 0, 0)


    def is_ceiling_furniture(self):
        return False


    def is_wall_furniture(self):
        return False


    def is_corner_furniture(self):
        return False


    # The furniture will be built where the bounding box corner in the direction of x-, y-, z- is at p
    def build(self, p, orient):
        return


class SmallBed(Furniture):
    def get_bounding_box_size(self, orient):
        return Utility.point3d(3, 3, 3)


    def is_wall_furniture(self):
        return True


    def build(self, p, orient):
        if orient is Map.z_plus:
            pos = ((p.x + 1, p.z + 2), (p.x + 1, p.z + 1))
            data = 0
        elif orient is Map.x_minus:
            pos = ((p.x, p.z + 1), (p.x + 1, p.z + 1))
            data = 1
        elif orient is Map.z_minus:
            pos = ((p.x + 1, p.z), (p.x + 1, p.z + 1))
            data = 2
        else:
            pos = ((p.x + 2, p.z + 1), (p.x + 1, p.z + 1))
            data = 3


        # bed head
        self.level.setBlockAt       (pos[0][0], p.y, pos[0][1], 26)
        self.level.setBlockDataAt   (pos[0][0], p.y, pos[0][1], 8 + data)
        # bed foot
        self.level.setBlockAt       (pos[1][0], p.y, pos[1][1], 26)
        self.level.setBlockDataAt   (pos[1][0], p.y, pos[1][1], data)


        if Districts.debug:
            self.level.setBlockAt       (p.x, p.y, p.z, 35)
            self.level.setBlockDataAt   (p.x, p.y, p.z, data)

        return


class Fridge(Furniture):
    def get_bounding_box_size(self, orient):
        if orient in Map.z_dirs:
            return Utility.point3d(1, 2, 3)
        else:
            return Utility.point3d(3, 2, 1)


    def is_wall_furniture(self):
        return True


    def build(self, p, orient):
        if orient is Map.z_plus:
            pos = ((p.x, p.z + 2), (p.x, p.z + 1))
            data = 3
        elif orient is Map.x_minus:
            pos = ((p.x, p.z), (p.x + 1, p.z))
            data = 0
        elif orient is Map.z_minus:
            pos = ((p.x, p.z), (p.x, p.z + 1))
            data = 1
        else:
            pos = ((p.x + 2, p.z), (p.x + 1, p.z))
            data = 2

        # bottom
        self.level.setBlockAt       (pos[0][0], p.y, pos[0][1], 42)
        self.level.setBlockDataAt   (pos[0][0], p.y, pos[0][1], 0)
        # top
        self.level.setBlockAt       (pos[0][0], p.y + 1, pos[0][1], 42)
        self.level.setBlockDataAt   (pos[0][0], p.y + 1, pos[0][1], 0)
        # bottom door
        self.level.setBlockAt       (pos[1][0], p.y, pos[1][1], 71)
        self.level.setBlockDataAt   (pos[1][0], p.y, pos[1][1], data)
        # bottom door
        self.level.setBlockAt       (pos[1][0], p.y + 1, pos[1][1], 71)
        self.level.setBlockDataAt   (pos[1][0], p.y + 1, pos[1][1], 8)

        if Districts.debug:
            self.level.setBlockAt       (p.x, p.y - 1, p.z, 35)
            self.level.setBlockDataAt   (p.x, p.y - 1, p.z, data)

        return


class Stair(Furniture):
    def __init__(self, level, height):
        Furniture.__init__(self, level)
        self.height = height


    def is_corner_furniture(self):
        return True


    def get_bounding_box_size(self, orient):
        if orient in Map.z_dirs:
            return Utility.point3d(1, self.height, self.height)
        else:
            return Utility.point3d(self.height, self.height, 1)


    def build(self, p, orient):
        Utility.fill(
            p, 
            self.get_bounding_box_size(orient).x, 
            self.get_bounding_box_size(orient).y, 
            self.get_bounding_box_size(orient).z,
            self.level,
            0,
            0)

        if orient is Map.z_plus:
            for i in range(self.height):
                self.level.setBlockAt       (p.x, p.y + i, p.z + i, 53)
                self.level.setBlockDataAt   (p.x, p.y + i, p.z + i, 2)
        elif orient is Map.x_minus:
            for i in range(self.height):
                self.level.setBlockAt       (p.x + self.height - i - 1, p.y + i, p.z, 53)
                self.level.setBlockDataAt   (p.x + self.height - i - 1, p.y + i, p.z, 1)
        elif orient is Map.z_minus:
            for i in range(self.height):
                self.level.setBlockAt       (p.x, p.y + i, p.z + self.height - i - 1, 53)
                self.level.setBlockDataAt   (p.x, p.y + i, p.z + self.height - i - 1, 3)
        else:
            for i in range(self.height):
                self.level.setBlockAt       (p.x + i, p.y + i, p.z, 53)
                self.level.setBlockDataAt   (p.x + i, p.y + i, p.z, 0)

        if Districts.debug:
            self.level.setBlockAt       (p.x, p.y, p.z, 35)
            self.level.setBlockDataAt   (p.x, p.y, p.z, self.level.blockDataAt(p.x, p.y, p.z))

        return


class Door(Furniture):
    def get_bounding_box_size(self, orient):
        return Utility.point3d(3, 2, 3)


    def is_wall_furniture(self):
        return True


    def build(self, p, orient):
        if orient is Map.z_plus:
            pos = (p.x + 1, p.z + 3)
            data = 3
        elif orient is Map.x_minus:
            pos = (p.x - 1, p.z + 1)
            data = 0
        elif orient is Map.z_minus:
            pos = (p.x + 1, p.z - 1)
            data = 1
        else:
            pos = (p.x + 3, p.z + 1)
            data = 2

        # bottom door
        self.level.setBlockAt       (pos[0], p.y, pos[1], 64)
        self.level.setBlockDataAt   (pos[0], p.y, pos[1], data)
        # bottom door
        self.level.setBlockAt       (pos[0], p.y + 1, pos[1], 64)
        self.level.setBlockDataAt   (pos[0], p.y + 1, pos[1], 8)