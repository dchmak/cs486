import time
from pymclevel import alphaMaterials, MCSchematic, MCLevel, BoundingBox
from mcplatform import *
from Utility import point3d
from Utility import point2d

roughness_factor = 2 # Slope that is considered rough

non_ground_blocks = [0, 17, 18, 161, 81, 78, 80, 31, 175, 38, 39, 40, 51] # air, logs, leaves, leaves, cactus, snow, snow_block, shrub, tall plant, flower, brown mushroom, red mushroom, fire
water_blocks = [8, 9, 79] # flowing water, water, ice

#global access point to map
map = None

#direction
x_plus = 0 # EAST
x_minus = ~x_plus # WEST
z_plus = 1 # SOUTH
z_minus = ~z_plus # NORTH
y_plus = 2 # UP
y_minus = ~y_plus # DOWN
directions = [x_plus, x_minus, z_plus, z_minus, y_plus, y_minus]
flat_dirs = [x_plus, x_minus, z_plus, z_minus]
x_dirs = [x_plus, x_minus]
z_dirs = [z_plus, z_minus]
y_dirs = [y_plus, y_minus]
parallel = {x_plus: z_dirs + y_dirs,
			x_minus: z_dirs + y_dirs,
			z_plus: x_dirs + y_dirs,
			z_minus: x_dirs + y_dirs,
			y_plus: x_dirs + z_dirs,
			y_minus: x_dirs + z_dirs}

#planning
empty = 0
road = 1
house = 2
lamp = 3
garden = 4
farm = 5

class Map:
	def __init__(self, level, box):
		self.level = level
		self.box = box
		self.analyze_height()
		self.analyze_water()
		self.analyze_roughness()
		map = self
		self.district = [[None for z in range(map.d)] for x in range(map.w)]
		self.districts = None

	def analyze_height(self):
		print("Generating height map")
		box = self.box
		level = self.level
		w = box.maxx - box.minx
		d = box.maxz - box.minz
		hmap = [[0 for z in range(d)] for x in range(w)]
		self.plan = [[empty for z in range(d)] for x in range(w)]

		minimum_height = None
		sum = 0

		for x in xrange(box.minx, box.maxx):
			for z in xrange(box.minz, box.maxz):
				for y in xrange(box.maxy, box.miny - 1, -1):
					if level.blockAt(x, y, z) not in non_ground_blocks or y == box.miny:
						height = y - box.miny
						hmap[x - box.minx][z - box.minz] = height
						sum += height
						if minimum_height is None or minimum_height > height:
							minimum_height = height
						break

		#SAVE DATA
		self.origin = point3d(box.minx, box.miny, box.minz)
		self.w = w
		self.h = box.maxy - box.miny
		self.d = d
		self.minimum_height = minimum_height
		self.average_height = sum / (w * d)
		self.height = hmap

	def analyze_water(self):
		print("Generating water map")
		box = self.box
		level = self.level
		w = self.w
		d = self.d
		wmap = [[False for z in range(d)] for x in range(w)]
		for x in xrange(box.minx, box.maxx):
			for z in xrange(box.minz, box.maxz):
				if level.blockAt(x, self.height[x - box.minx][z - box.minz] + box.miny, z) in water_blocks:
					wmap[x - box.minx][z - box.minz] = True
		self.water = wmap

	def analyze_roughness(self):
		self.roughness = [[0 for z in range(self.d)] for x in range(self.w)]
		self.analyze_x_roughness_map()
		self.analyze_z_roughness_map()

	def analyze_x_roughness_map(self):
		print("Generating x roughness map")
		w = self.w - 1  # Shorter in x direction, as it is between x values
		d = self.d
		xrmap = [[False for z in range(d)] for x in range(w)]
		for x in range(w):
			for z in range(d):
				delta = abs(self.height[x][z] - self.height[x + 1][z])  # compares across x
				if (delta >= roughness_factor):
					self.roughness[x][z] += 1
					self.roughness[x + 1][z] += 1
					xrmap[x][z] = True
		self.x_roughness = xrmap

	def analyze_z_roughness_map(self):
		print("Generating z roughness map")
		w = self.w
		d = self.d - 1  # Shorter in z direction, as it is between z values
		zrmap = [[False for z in range(d)] for x in range(w)]
		for x in range(w):
			for z in range(d):
				delta = abs(self.height[x][z] - self.height[x][z + 1])  # compares across z
				if (delta >= roughness_factor):
					self.roughness[x][z] += 1
					self.roughness[x][z + 1] += 1
					zrmap[x][z] = True
		self.z_roughness = zrmap

	def point2d_to_int(self, point):
		return self.w * point.z + point.x

	def int_to_point2d(self, int):
		return point2d(int % self.w, int // self.w)


	def get_average_height(self, p1, p2):
		total = 0
		sum = 0
		x = p1.x
		z = p2.z

		while x <= p2.x:
			while z <= p2.z:
				total += 1
				sum += self.height[x][z]
				z += 1
			x += 1

		return (sum // total)


class Box:
	def __init__(self, p1, p2):
		self.minx = p1.x
		self.miny = p1.y
		self.minz = p1.z
		self.maxx = p2.x
		self.maxy = p2.y
		self.maxz = p2.z
