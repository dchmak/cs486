import random
import time
import Map
import Roof
import Pillars
import Floor
import Room
import Roads
import Districts
import Search
import Garden
import Farm

from pymclevel import alphaMaterials, MCSchematic, MCLevel, BoundingBox
from mcplatform import *
from random import Random
from Cells import *
from Utility import *
import random
from numpy import genfromtxt
import numpy as np


excelFilePath = r'stock-filters\cs486\Filters\ryansPallette.csv'
biomeFilePath = r'stock-filters\cs486\Filters\biomeTranslater.csv'


distance_power = 1.3 # The power used for distance calculation. Manhattan distance is 1, normal distance is 2.
                    # The point is to penalize weirdly long buildings by raising the cost for longer distances

cell_new_cost = 0.5 # Chance of generating new cell
cell_distance_cost = 0.7 # Cost per distance from main cell
cell_symmetry_bonus = 3 # Bonus for chance if cell is symmetric
cell_radial_bonus = 2 # Bonus for radial symmetry
cell_height_cost = 0.5 # Percent chance of building being built for each y value

max_attempt = 5

assign_diminish_rate = 0.8

cell_dim = point3d(9, 5, 9)

finalPallette = [(19,0), (35,4), (95,4), (159,4)]
pillarsData = [(19,0), (35,4), (95,4), (159,4)]

foundation_material = 4
foundation_data = 0

wall_material = 4
wall_data = 0

roof_material = 4
roof_data = 0

pillar_material = 4
pillar_data = 0

fence_material = 85
fence_data = 0

window_chance = 0.33
garden_chance = 0.33

class House:
    def __init__(self, district):
        self.cells = []
        self.plant_seed = random.randint(0, 2)
        self.district = district

    # Can use none to leave blanks, so (1, 0, 1) can be found with (1, None, None)
    def has_cell(self, cell_point):
        for cell in self.cells:
            if (cell.cell_point.x == None or cell.cell_point.x == cell_point.x) and \
                    (cell.cell_point.y == None or cell.cell_point.y == cell_point.y) and \
                    (cell.cell_point.z == None or cell.cell_point.z == cell_point.z):
                return True
        return False


    def get_cell(self, cell_point):
        for cell in self.cells:
            if cell.cell_point.x == cell_point.x and \
                    cell.cell_point.y == cell_point.y and \
                    cell.cell_point.z == cell_point.z:
                return cell
        return None


# the following code is to load in the block information and to build a pallette. Should be moved to a
# more general area but for now will sit here while it is built

# returns rgb values, index 0 is the rgb, index 1 are the blocks associated
def readData(fileName):
    data = genfromtxt(fileName, delimiter=',', dtype=None)
    rgbValues = []

    for line in data:
            rgbs = line[2].split(';')
            associatedBlocks = []
            blocks = line[4:]
            for block in blocks:
                if block == '':
                    break
                values = block.split(';')
                associatedBlocks.append((values[0],values[1]))
            rgbValues.append((rgbs, associatedBlocks))
    #print("rgb 0:", rgbValues[0])
    return rgbValues


def getBasePallette(level, box, rgbValues):
    biomesTranslator = genfromtxt(biomeFilePath, delimiter=',', dtype=None)
    chunk = level.getChunk(box.minx/16, box.minz/16)
    chunk.dirty = True
    arr = chunk.root_tag["Level"]["Biomes"].value
    biomes = set(arr)
    baseRGBs = []
    pillars = []
    fences = []
    for biome in biomes:
        print("adding biome: ", biome)
        if(biome > 35):
            print("biome too rare, choosing default base")
            baseRGBs.append(['127','178','56'])
            pillars.append(['5','0'])
            fences.append(['85','0'])
        else:
            values = biomesTranslator[biome][3].split(';')
            pillarBlock = biomesTranslator[biome][4].split(';')
            fenceBlock = biomesTranslator[biome][5].split(';')
            baseRGBs.append(values)
            pillars.append(pillarBlock)
            fences.append(fenceBlock)
    basePallette = []
    for rgbValue in rgbValues:
        if rgbValue[0] in baseRGBs:
            for block in rgbValue[1]:
                basePallette.append(block)
    return baseRGBs, basePallette, pillars, fences


def sigmoid(inputs):
    output = 1/(1 + np.exp(-inputs))
    #output = np.exp(inputs)/(np.exp(inputs)+1)
    return output


def predict_logistic_regression(inputs, weights, threshold):
    predicted_values = [];
    for currInput in inputs:
        yVal = sigmoid(np.matmul(weights.transpose(), currInput));
        if yVal >= threshold:
            predicted_values.append(1)
        else:
            predicted_values.append(0)
    return predicted_values


def eval_logistic_regression(inputs, weights, labels, threshold):
    nlog = 0;
    correct = 0.0;
    predictions = predict_logistic_regression(inputs, weights, threshold);
    newRGBs = []
    for index, val in enumerate(predictions):
        if val == 1:
            newRGBs.append(inputs[index])

    return newRGBs


def calculateHessian(train_inputs, weights, hyperParam):
    sigmoids = [];
    for train_input in train_inputs:
        z = np.matmul(weights.transpose(), train_input)
        currSigmoid = sigmoid(z)
        sigmoids.append(currSigmoid*(1-currSigmoid))
    R = np.diag(sigmoids)
    hessian = np.matmul(train_inputs.transpose(), R)
    hessian = np.matmul(hessian, train_inputs);
    lambdai = np.identity(len(hessian)) * hyperParam
    hessianLambda = np.add(hessian, lambdai)
    return hessianLambda


def calculateGradient(train_inputs, train_labels, weights):
    gradient = np.zeros(len(train_inputs[0]))
    for i in range(len(train_inputs)):
        currSigmoid = sigmoid(np.matmul(weights.transpose(), train_inputs[i]))
        factor = currSigmoid - train_labels[i]
        valueN = factor * train_inputs[i]
        gradient = np.add(gradient, valueN)
    return gradient;


def train_logistic_regression(train_inputs, train_labels, lambda_hyperparam):
    weights = initialize_weights(train_inputs.shape[1])
    count = 0;
    while count < 2:
        matrixH = calculateHessian(train_inputs, weights, lambda_hyperparam)
        gradientL = calculateGradient(train_inputs, train_labels, weights)
        oldWeights = weights;
        weights = oldWeights - np.matmul(np.linalg.inv(matrixH),gradientL)

        max_diff = np.absolute(weights - oldWeights).max()
        if max_diff < 0.001:
            count += 1
        else:
            count = 0
    return weights


def initialize_weights(n_weights):
    returnWeights = []
    for weight in range(n_weights):
        returnWeights.append(np.random.uniform(-0.05,0.05))
    return np.array(returnWeights).transpose()


def makeData(baseRGBs, rgbValues):
    labels = []
    inputs = []
    for rgbValue in rgbValues:
        inputs.append(rgbValue[0])
        if rgbValue[0] in baseRGBs:
            labels.append(1)
        else:
            labels.append(0)
    inputs = np.array(inputs, dtype='float64')
    labels = np.array(labels)
    return inputs, labels


# baseRGBs = all the rbgs in the base pallette, rgbValues  = all the data
def buildPallette(baseRGBs, basePallette, rgbValues, threshold):
    trainingInputs, trainingLabels = makeData(baseRGBs, rgbValues)
    weights = train_logistic_regression(trainingInputs, trainingLabels, 200)
    newRGBs = eval_logistic_regression(trainingInputs, weights, trainingLabels, threshold)
    finalRGBs = baseRGBs[:]
    for newRGB in newRGBs:
        if newRGB not in baseRGBs:
            newList = []
            x = newRGB.tolist();
            y = map(int, x)
            for i in y:
                newList.append(str(i))
            finalRGBs.append(newList)
    print("added ", len(finalRGBs) - len(baseRGBs), " rgb values")
    finalPallette = []
    for rgb in rgbValues:
        if rgb[0] in finalRGBs:
            for block in rgb[1]:
                finalPallette.append(block)
    return finalPallette


def buildPallette2(baseRGBs, basePallette, rgbValues, threshold):
    trainingInputs, trainingLabels = makeData(baseRGBs, rgbValues)
    print("inputs:", trainingLabels)
    weights = train_logistic_regression(trainingInputs, trainingLabels, 1)
    print("weights", weights)
    newRGBs = eval_logistic_regression(trainingInputs, weights, trainingLabels, threshold)
    print("newRGBs", newRGBs)


def setBuildingMaterials(district):
    x = random.randint(0, len(finalPallette)-1)
    district.wall_material = finalPallette[x][0]
    district.wall_data = finalPallette[x][1]

    y = random.randint(0, len(finalPallette)-1)
    district.foundation_material = finalPallette[y][0]
    district.foundation_data = finalPallette[y][1]

    z = random.randint(0, len(finalPallette)-1)
    district.roof_material = finalPallette[z][0]
    district.roof_data = finalPallette[z][1]

    a = random.randint(0, len(pillarsData)-1)
    district.pillar_material = pillarsData[a][0]
    district.pillar_data = pillarsData[a][1]

    b = random.randint(0, len(fencesData)-1)
    district.fence_material = fencesData[b][0]
    district.fence_data = fencesData[b][1]

    print("district " + str(district.name) + " pallete info:", district.wall_material, district.foundation_material, district.pillar_material)

    #print('set to:', wall_material, foundation_material)


def perform(level, box, options):
    map = Map.Map(level, box)
    load_palette(map, box)

    if Districts.debug:
        print("Building house (%i to %i, %i to %i, %i to %i)" % (box.minx, box.maxx, box.miny, box.maxy, box.minz, box.maxz))
        fill(point3d(box.minx, box.miny, box.minz), box.maxx - box.minx, box.maxy - box.miny, box.maxz - box.minz, level, 0, 0)


    build_house(box.minx + 5, box.minz + 5, map)


def load_palette(map, box):
    print("--------")
    print("reading data in...")
    rgbValues = readData(excelFilePath)
    global finalPallette, pillarsData, fencesData
    baseRGBs, basePallette, pillarsData, fencesData  = getBasePallette(map.level, box, rgbValues)
    #threshold is how similar we want the outputs to be
    threshold = 0.1
    finalPallette = buildPallette(baseRGBs, basePallette, rgbValues, threshold)
    for district in map.districts:
        setBuildingMaterials(district)


# x, z are map relative
def build_house(x, z, map):
    print('--------\nStart Build House at (%d, %d)' % (x, z))
    p1 = point2d(x, z)
    p2 = point2d(x + cell_dim.z, z + cell_dim.z)

    if x < 0 or x + cell_dim.x > map.w - 1 or z < 0 or z + cell_dim.z > map.d - 1:
        # no can do
        print("Out of bounds")
        return

    district = map.district[x][z]
    if district == None: # House must be in district
        return

    y = map.get_average_height(p1, p2)
    print("y=" + str(y))

    if y < 0 or y + cell_dim.y > map.h - 1:
        # no can do
        print("Out of bounds")
        return

    house = House(district)
    point = point3d(x + map.origin.x, y + map.origin.y, z + map.origin.z)
    cell_point = point3d(0, 0, 0)
    print("Populating a building at (%i %i %i)" % (point.x, point.y, point.z))


    r = random.randint(0, 4)
    type = building
    if r == 4 or (district.type == Districts.farming and r < 3):
        type = farm

    populate_cell(point, cell_point, cell_dim, house, map, type)

    # some form of generation

    print('Clearing out space and building Cell...')
    clear_and_build(house, map)

    print('Decorating Cell...')
    decorate(house, map)

    print('Placing Door...')
    choices = list(filter(lambda cell: cell.cell_point.y == 0, house.cells))
    if len(choices) > 0:
        cell = random.choice(choices)

        if cell.type == building:
            p = cell.p1.shift_z(cell.size.z // 2)
            d = distance3d(p, map.origin + district.origin)
            o = Map.x_minus

            p = cell.p1.shift_x(cell.size.x // 2)
            z_minus_d = distance3d(p, map.origin + district.origin)
            if z_minus_d < d:
                d = z_minus_d
                o = Map.z_minus

            p = cell.p1.shift_z(cell.size.z // 2).shift_x(cell.size.x - 1)
            x_plus_d = distance3d(p, map.origin + district.origin)
            if x_plus_d < d:
                d = x_plus_d
                o = Map.x_plus

            p = cell.p1.shift_x(cell.size.x // 2).shift_z(cell.size.z - 1)
            z_plus_d = distance3d(p, map.origin + district.origin)
            if z_plus_d < d:
                d = z_plus_d
                o = Map.z_plus

            door_room = Room.DoorRoom(house, map.level, [o])

            door_room.assign_cells(choices)
            door_room.populate(True)
            coord = door_room.door_coordinates
            orientation = door_room.door_orientation

            if coord == None: # can't make path
                return

            if orientation == Map.x_plus:
                point = point2d(coord.x + 2 - map.origin.x, coord.z - map.origin.z)
            if orientation == Map.x_minus:
                point = point2d(coord.x - 2  - map.origin.x, coord.z  - map.origin.z)
            if orientation == Map.z_plus:
                point = point2d(coord.x  - map.origin.x, coord.z + 2  - map.origin.z)
            if orientation == Map.z_minus:
                point = point2d(coord.x  - map.origin.x, coord.z - 2  - map.origin.z)
        else:
            point = point2d(cell.p1.x - map.origin.x + (cell.size.x // 2) + 1, cell.p1.z - map.origin.z + (cell.size.z // 2) + 1)

        if 0 <= point.x < map.w and 0 <= point.z < map.d:
            Roads.add_path(point, map)


def clear_and_build(house, map):
    for cell in house.cells:
        fill(cell.p1, cell.size.x, cell.size.y, cell.size.z, map.level, 0, 0)
        build_cell(cell, map, map.minimum_height + map.box.miny)


def build_cell(cell, map, miny):
    if cell.type == building:
        build_building_cell(cell, map, miny)
    elif cell.type == garden:
        Garden.build_garden_cell(cell, map, miny)
    elif cell.type == farm:
        Farm.build_farm_cell(cell, map, miny)

def build_building_cell(cell, map, miny):
    p1 = cell.p1
    p2 = cell.p2

    wall_material = cell.house.district.wall_material
    wall_data = cell.house.district.wall_data
    foundation_material = cell.house.district.foundation_material
    foundation_data = cell.house.district.foundation_data
    fence_material = cell.house.district.fence_material
    fence_data = cell.house.district.fence_data
    roof_material = cell.house.district.roof_material
    roof_data = cell.house.district.roof_data
    pillar_material = cell.house.district.pillar_material
    pillar_data = cell.house.district.pillar_material

    # set planning
    for plan_x in xrange(min((p1.x, p2.x)), max((p1.x, p2.x))):
        for plan_z in xrange(min((p1.z, p2.z)), max((p1.z, p2.z))):
            map.plan[plan_x - map.origin.x][plan_z - map.origin.z] = Map.house

    # build upper floor
    Floor.build_floor(cell, map, foundation_material, foundation_data)

    #build walls
    if cell.neighbours[Map.x_minus] is None or cell.neighbours[Map.x_minus].type != building:
        p = point3d(p1.x + 1, p1.y, p1.z + 1)
        fill(p, 1, cell.size.y, cell.size.z - 2, map.level, wall_material, wall_data)
        # build wall corners
        if cell.neighbours[Map.z_plus] is not None:
            p = point3d(p1.x + 1, p1.y, p2.z - 1)
            fill(p, 1, cell.size.y, 1, map.level, wall_material, wall_data)
        if cell.neighbours[Map.z_minus] is not None:
            p = point3d(p1.x + 1, p1.y, p1.z)
            fill(p, 1, cell.size.y, 1, map.level, wall_material, wall_data)

        # WINDOW TIME
        rn = Random()
        if rn.random() < window_chance:
            fill(point3d(p1.x + 1, p1.y + 2, p1.z + (cell.size.z // 2)), 1, 2, 1, map.level, fence_material, fence_data)

    if cell.neighbours[Map.x_plus] is None or cell.neighbours[Map.x_plus].type != building:
        p = point3d(p2.x - 2, p1.y, p1.z + 1)
        fill(p, 1, cell.size.y, cell.size.z - 2, map.level, wall_material, wall_data)
        # build wall corners
        if cell.neighbours[Map.z_plus] is not None:
            p = point3d(p2.x - 2, p1.y, p2.z - 1)
            fill(p, 1, cell.size.y, 1, map.level, wall_material, wall_data)
        if cell.neighbours[Map.z_minus] is not None:
            p = point3d(p2.x - 2, p1.y, p1.z)
            fill(p, 1, cell.size.y, 1, map.level, wall_material, wall_data)

        # WINDOW TIME
        rn = Random()
        if rn.random() < window_chance:
            fill(point3d(p2.x - 2, p1.y + 2, p1.z + (cell.size.z // 2)), 1, 2, 1, map.level, fence_material,
                 fence_data)

    if cell.neighbours[Map.z_minus] is None or cell.neighbours[Map.z_minus].type != building:
        p = point3d(p1.x + 1, p1.y, p1.z + 1)
        fill(p, cell.size.x - 2, cell.size.y, 1, map.level, wall_material, wall_data)
        # build wall corners
        if cell.neighbours[Map.x_plus] is not None:
            p = point3d(p2.x - 1, p1.y, p1.z + 1)
            fill(p, 1, cell.size.y, 1, map.level, wall_material, wall_data)
        if cell.neighbours[Map.x_minus] is not None:
            p = point3d(p1.x, p1.y, p1.z + 1)
            fill(p, 1, cell.size.y, 1, map.level, wall_material, wall_data)

        # WINDOW TIME
        rn = Random()
        if rn.random() < window_chance:
            fill(point3d(p1.x + (cell.size.x // 2), p1.y + 2, p1.z + 1), 1, 2, 1, map.level, fence_material,
                 fence_data)

    if cell.neighbours[Map.z_plus] is None or cell.neighbours[Map.z_plus].type != building:
        p = point3d(p1.x + 1, p1.y, p2.z - 2)
        fill(p, cell.size.x - 2, cell.size.y, 1, map.level, wall_material, wall_data)
        if cell.neighbours[Map.x_plus] is not None:
            p = point3d(p2.x - 1, p1.y, p2.z - 2)
            fill(p, 1, cell.size.y, 1, map.level, wall_material, wall_data)
        if cell.neighbours[Map.x_minus] is not None:
            p = point3d(p1.x, p1.y, p2.z - 2)
            fill(p, 1, cell.size.y, 1, map.level, wall_material, wall_data)

        # WINDOW TIME
        rn = Random()
        if rn.random() < window_chance:
            fill(point3d(p1.x + (cell.size.x // 2), p1.y + 2, p2.z - 2), 1, 2, 1, map.level, fence_material,
                 fence_data)

    #build pillars
    Pillars.build_pillars(cell, map, pillar_material, pillar_data)

    if cell.neighbours[Map.y_plus] is None:
        Roof.build_roof(cell, map, roof_material, roof_data, foundation_material, foundation_data, wall_material, wall_data)

    if Districts.debug:
        map.level.setBlockAt       (p1.x, p1.y, p1.z, 20)
        map.level.setBlockDataAt   (p1.x, p1.y, p1.z, 0)


def maybe_populate_cell(point, cell_point, size, house, map, type):
    # don't double generate
    if house.has_cell(cell_point):
        return

    chance = cell_new_cost
    chance *= pow(cell_height_cost, cell_point.y)

    #DISTANCE COSTS
    dist = distance3d(cell_point, point3d(0, 0, 0), distance_power)
    chance *= pow(cell_distance_cost, dist)

    #SYMMETRY BONUSES
    if house.has_cell(point3d(cell_point.x * -1, cell_point.y, cell_point.z)):
        chance *= cell_symmetry_bonus
    if house.has_cell(point3d(cell_point.x, cell_point.y, cell_point.z * -1)):
        chance *= cell_symmetry_bonus
    if house.has_cell(point3d(cell_point.x * -1, cell_point.y, cell_point.z * -1)):
        chance *= cell_radial_bonus

    #Give small farms a big boost
    if type == farm and len(house.cells) < 4:
        chance *= 3

    rn = Random()
    if rn.random() < chance:
        populate_cell(point, cell_point, size, house, map, type)
    #TODO better probability


def populate_cell(point, cell_point, size, house, map, type):
    # Can't make cells out of bounds
    if not is_in_bounds(point.x, point.y, point.z, size, map):
        return

    # Can't make cells in occupied territory
    if is_occupied(point.x, point.y, point.z, size, map):
        return

    # Can't make house in ground
    if is_ground(point.x, point.y, point.z, size, map):
        return

    # Farms and gardens must be on ground
    if house.has_cell(point3d(cell_point.x, cell_point.y - 1, cell_point.z)) and type is not building:
        return

    # Can't make floating cells
    floating = not is_ground(point.x, point.y - size.y + 1, point.z, size, map) and not house.has_cell(point3d(cell_point.x, cell_point.y - 1, cell_point.z))
    if floating:
        return

    cell = Cell(point, cell_point, size, type, house)

    if type is building:
        maybe_populate_cell(point.shift_y(size.y - 1), cell_point.shift_y(1), size, house, map, type)
    if type is building:
        maybe_populate_cell(point.shift_y(-1 * (size.y - 1)), cell_point.shift_y(-1), size, house, map, type)

    #CURRENTLY REMOVED FOR LOGIC PURPOSES
    #possibility of gardens
    #rn = Random()
    #if type is building and rn.random() < garden_chance:
    #    type = garden

    maybe_populate_cell(point.shift_x(size.x - 1), cell_point.shift_x(1), size, house, map, type)
    maybe_populate_cell(point.shift_x(-1 * (size.x - 1)), cell_point.shift_x(-1), size, house, map, type)
    maybe_populate_cell(point.shift_z(size.z - 1), cell_point.shift_z(1), size, house, map, type)
    maybe_populate_cell(point.shift_z(-1 * (size.z - 1)), cell_point.shift_z(-1), size, house, map, type)


def decorate(house, map):
    print('Decorate stairs')
    cell_y = 1
    while True:
        cells = set(filter(lambda cell: cell.cell_point.y == cell_y, house.cells))
        if len(cells) <= 0:
            break

        while len(cells) > 0:
            neightbour_filter = lambda neighbour: neighbour is not None and neighbour.cell_point.y == cell_y
            get_successor = lambda cell: list(filter(neightbour_filter, cell.neighbours.values()))
            choices = Search.bfs(next(iter(cells)), get_successor)
            cells -= choices

            cell = random.choice(list(choices)).neighbours[Map.y_minus]
            stair_room = Room.StairRoom(house, map.level, cell.size.y - 1)
            stair_room.assign_cells([cell])
            stair_room.populate(True)

        cell_y += 1


    print('Decorate Bedroom...')
    bedroom = Room.Bedroom(house, map.level)
    cell_list = find_cells_to_assign(house, map, bedroom)
    if cell_list is None or len(cell_list) is 0:
        print('Cannot find room to be bedroom') # should not happen
    else:
        bedroom.assign_cells(cell_list, 0)
        bedroom.populate()

    print('Decorate Kitchen...')
    kitchen = Room.Kitchen(house, map.level)
    cell_list = find_cells_to_assign(house, map, kitchen)
    if cell_list is None or len(cell_list) is 0:
        print('Cannot find room to be kitchen') # should not happen
    else:
        kitchen.assign_cells(cell_list, 1)
        kitchen.populate()


def find_cells_to_assign(house, map, room):
    attempt = 0
    while attempt < max_attempt:
        choices = filter(lambda cell: not cell.assigned, house.cells)
        if len(choices) <= 0:
            break

        cell = random.choice(choices)

        cell_list = recursive_find_cells_to_assign(cell, room, map, 0)
        if cell_list is not None and len(cell_list) > 0:
            if Districts.debug:
                for cell in cell_list:
                    map.level.setBlockAt    (cell.p1.x, cell.p1.y, cell.p1.z, 35)
                    map.level.setBlockDataAt(cell.p1.x, cell.p1.y, cell.p1.z, 13)
                    map.level.setBlockAt    (cell.p2.x, cell.p2.y, cell.p2.z, 35)
                    map.level.setBlockDataAt(cell.p2.x, cell.p2.y, cell.p2.z, 14)

            return cell_list

        attempt += 1


def recursive_find_cells_to_assign(cell, room, map, depth):
    if (cell is None or
        cell.assigned or
        not is_in_bounds(cell.p1.x, cell.p1.y, cell.p1.z, cell_dim, map)):
        return []

    cell_list = [cell]
    cell.assigned = True

    if random.random() < (assign_diminish_rate ** depth) * room.get_multiple_cells_probability():
        direction = random.choice(Map.flat_dirs)
        cell_list += recursive_find_cells_to_assign(cell.neighbours[direction], room, map, depth + 1)

    return cell_list
