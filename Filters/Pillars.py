import Utility


def build_pillars(cell, map, pillar_material, pillar_data):
    level = map.level
    p1 = cell.p1
    p2 = cell.p2

    Utility.fill(p1, 1, cell.size.y, 1, level, pillar_material, pillar_data)
    Utility.fill(Utility.point3d(p2.x - 1, p1.y, p1.z), 1, cell.size.y, 1, level, pillar_material, pillar_data)
    Utility.fill(Utility.point3d(p1.x, p1.y, p2.z - 1), 1, cell.size.y, 1, level, pillar_material, pillar_data)
    Utility.fill(Utility.point3d(p2.x - 1, p1.y, p2.z - 1), 1, cell.size.y, 1, level, pillar_material, pillar_data)
