from pymclevel import alphaMaterials, MCSchematic, MCLevel, BoundingBox
from mcplatform import *

def fill(p1, w, h, d, level, block_id, block_data):
    for x in range(w):
        for y in range(h):
            for z in range(d):
                level.setBlockAt(int(p1.x + x), int(p1.y + y), int(p1.z + z), block_id)
                level.setBlockDataAt(int(p1.x + x), int(p1.y + y), int(p1.z + z), block_data)


def is_empty(p1, w, h, d, level):
    for x in range(w):
        for y in range(h):
            for z in range(d):
                if level.blockAt(int(p1.x + x), int(p1.y + y), int(p1.z + z)) != 0:
                    return False

    return True


def is_full(p1, w, h, d, level):
    for x in range(w):
        for y in range(h):
            for z in range(d):
                if level.blockAt(int(p1.x + x), int(p1.y + y), int(p1.z + z)) == 0:
                    return False

    return True

#Uses Z to fit with minecraft dimensions
class point2d:
    def __init__(self, x, z):
        self.x = x
        self.z = z

    def __add__(self, point):
        return point2d(self.x + point.x, self.z + point.z)

class point3d(point2d):
    def __init__(self, x, y, z):
        self.x = x
        self.z = z
        self.y = y

    def shift_x(self, value):
        return point3d(self.x + value, self.y, self.z)

    def shift_y(self, value):
        return point3d(self.x, self.y + value, self.z)

    def shift_z(self, value):
        return point3d(self.x, self.y, self.z + value)

    def __add__(self, point):
        return point3d(self.x + point.x, self.y + point.y, self.z + point.z)



    def __repr__(self):
        return '(%d, %d, %d)' % (self.x, self.y, self.z)


    def __str__(self):
        return '(%d, %d, %d)' % (self.x, self.y, self.z)


def distance2d(p1, p2, distance_power=2.0):
    pow_sum = pow(abs(p1.x - p2.x), distance_power) + \
              pow(abs(p1.z - p2.z), distance_power)
    return pow(pow_sum, 1 / distance_power)

def distance3d(p1, p2, distance_power=2.0):
    pow_sum = pow(abs(p1.x - p2.x), distance_power) + \
              pow(abs(p1.y - p2.y), distance_power) + \
              pow(abs(p1.z - p2.z), distance_power)
    return pow(pow_sum, 1 / distance_power)
