import Map
from pymclevel import alphaMaterials, MCSchematic, MCLevel, BoundingBox
from mcplatform import *
from Utility import *

free_ratio = 0.4 # Proportion of cell filled with non-ground blocks for it to be considered free

# cell type
building = 0
garden = 1
farm = 2

class Cell:
    # x, y, z are absolute, not relative to map
    def __init__(self, point, cell_point, size, type, house):
        self.type = type
        self.house = house
        self.size = size
        self.p1 = point
        self.cell_point = cell_point
        self.p2 = point3d(point.x + size.x, point.y + size.y, point.z + size.z)
        self.create_neighbours()
        house.cells.append(self)

        self.assigned = False

    def create_neighbours(self):
        p = self.cell_point
        house = self.house
        # Look around for neighbours
        self.neighbours = {
            Map.x_plus: house.get_cell(point3d(p.x + 1, p.y, p.z)),
            Map.x_minus: house.get_cell(point3d(p.x - 1, p.y, p.z)),
            Map.z_plus: house.get_cell(point3d(p.x, p.y, p.z + 1)),
            Map.z_minus: house.get_cell(point3d(p.x, p.y, p.z - 1)),
            Map.y_plus: house.get_cell(point3d(p.x, p.y + 1, p.z)),
            Map.y_minus: house.get_cell(point3d(p.x, p.y - 1, p.z))
        }

        # Introduce yourself to the neighbours
        if self.neighbours[Map.x_plus] is not None:
            self.neighbours[Map.x_plus].neighbours[Map.x_minus] = self
        if self.neighbours[Map.x_minus] is not None:
            self.neighbours[Map.x_minus].neighbours[Map.x_plus] = self
        if self.neighbours[Map.z_plus] is not None:
            self.neighbours[Map.z_plus].neighbours[Map.z_minus] = self
        if self.neighbours[Map.z_minus] is not None:
            self.neighbours[Map.z_minus].neighbours[Map.z_plus] = self
        if self.neighbours[Map.y_plus] is not None:
            self.neighbours[Map.y_plus].neighbours[Map.y_minus] = self
        if self.neighbours[Map.y_minus] is not None:
            self.neighbours[Map.y_minus].neighbours[Map.y_plus] = self

# Absolute x, y, z
def is_in_bounds(x, y, z, size, map):
    return not (x - map.origin.x < 0 or x - map.origin.x + size.x >= map.w or
        y - map.origin.y < 0 or y - map.origin.y + size.y >= map.h or
        z - map.origin.z < 0 or z - map.origin.z + size.z >= map.d)

# Absolute x, y, z
def is_ground(x, y, z, size, map):
    total = 0
    non_ground = 0

    for xval in range(size.x):
        for zval in range(size.z):
            # size controls for y in case we are too low
            for yval in range(min(size.y, y + size.y)):
                px = xval + x
                pz = zval + z
                py = yval + max(y, 0)

                # Count ground blocks
                total += 1
                if map.level.blockAt(px, py, pz) in Map.non_ground_blocks:
                    non_ground += 1

    return (100 * non_ground / total) < (free_ratio * 100)

# Absolute x, y, z
def is_occupied(x, y, z, size, map):
    for xval in range(size.x):
        for zval in range(size.z):
            px = xval + x
            pz = zval + z

            # Plan problem
            if map.plan[px - map.origin.x][pz - map.origin.z] != Map.empty or map.water[px - map.origin.x][pz - map.origin.z]:
                return True
    return False


# Absolute x, y, z
def is_free(x, y, z, size, map):
    return not is_ground(x, y, z, size, map) and not is_in_bounds(x, y, z, size, map) and not is_occupied(x, y, z, size, map)
