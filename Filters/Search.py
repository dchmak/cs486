import collections


def bfs(initial_state, get_successors):
    visited = set()
    frontier = collections.deque([[initial_state]])

    while len(frontier) != 0:
        selected = frontier.popleft()
        visited.add(selected[-1])

        successors = get_successors(selected[-1])
        for successor in successors:
            if successor not in visited:
                frontier.append(selected + [successor])

    return visited