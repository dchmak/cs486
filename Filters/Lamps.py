from pymclevel import alphaMaterials, MCSchematic, MCLevel, BoundingBox
from mcplatform import *
import Map
import House

def lamp_it_up(map):
    for i in range((map.w // 2) * (map.d // 2) * 4):
        x = i % map.w
        z = i // map.w
        if x % 2 == z % 2:
            if map.plan[x][z] == Map.empty and neighbour_is(x, z, Map.road, map) and not neighbour_is(x, z, Map.lamp, map):
                if not neighbour_is(x + 1, z, Map.lamp, map) and not neighbour_is(x - 1, z, Map.lamp, map) and not neighbour_is(x, z + 1, Map.lamp, map) and not neighbour_is(x, z - 1, Map.lamp, map):
                    build_lamp(x, z, map)

def neighbour_is(x, z, val, map):
    if x > 0 and map.plan[x - 1][z] == val:
        return True
    if x < map.w - 1 and map.plan[x + 1][z] == val:
        return True
    if z > 0 and map.plan[x][z - 1] == val:
        return True
    if z < map.d - 1 and map.plan[x][z + 1] == val:
        return True
    return False

def build_lamp(x, z, map):
    map.plan[x][z] = Map.lamp
    y = map.height[x][z]
    map.level.setBlockAt(x + map.origin.x, y + map.origin.y + 1, z + map.origin.z, House.fence_material)
    map.level.setBlockDataAt(x + map.origin.x, y + map.origin.y + 1, z + map.origin.z, House.fence_data)
    map.level.setBlockAt(x + map.origin.x, y + map.origin.y + 2, z + map.origin.z, House.fence_material)
    map.level.setBlockDataAt(x + map.origin.x, y + map.origin.y + 2, z + map.origin.z, House.fence_data)
    map.level.setBlockAt(x + map.origin.x, y + map.origin.y + 3, z + map.origin.z, 50)
    map.level.setBlockDataAt(x + map.origin.x, y + map.origin.y + 3, z + map.origin.z, 5)