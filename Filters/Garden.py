import Map
import Districts
import House
from pymclevel import alphaMaterials, MCSchematic, MCLevel, BoundingBox
from mcplatform import *
from Utility import *

def build_garden_cell(cell, map, miny):
    p1 = cell.p1
    p2 = cell.p2

    wall_material = cell.house.district.wall_material
    wall_data = cell.house.district.wall_data
    foundation_material = cell.house.district.foundation_material
    foundation_data = cell.house.district.foundation_data
    fence_material = cell.house.district.fence_material
    fence_data = cell.house.district.fence_data
    roof_material = cell.house.district.roof_material
    roof_data = cell.house.district.roof_data

    # set planning and height
    for plan_x in xrange(min((p1.x, p2.x)), max((p1.x, p2.x))):
        for plan_z in xrange(min((p1.z, p2.z)), max((p1.z, p2.z))):
            map.plan[plan_x - map.origin.x][plan_z - map.origin.z] = Map.farm
            map.height[plan_x - map.origin.x][plan_z - map.origin.z] = p1.y - map.origin.y

    # fill with grass
    fill(p1, cell.size.x, 1, cell.size.z, map.level, 2, 0)

    # build walls
    if cell.neighbours[Map.x_minus] is None:
        p = point3d(p1.x + 1, p1.y + 1, p1.z + 1)
        fill(p, 1, 1, cell.size.z - 2, map.level, House.fence_material, House.fence_data)
        # build wall corners
        if cell.neighbours[Map.z_plus] is not None:
            p = point3d(p1.x + 1, p1.y + 1, p2.z - 1)
            fill(p, 1, 1, 1, map.level, House.fence_material, House.fence_data)
        if cell.neighbours[Map.z_minus] is not None:
            p = point3d(p1.x + 1, p1.y + 1, p1.z)
            fill(p, 1, 1, 1, map.level, House.fence_material, House.fence_data)

    if cell.neighbours[Map.x_plus] is None:
        p = point3d(p2.x - 2, p1.y + 1, p1.z + 1)
        fill(p, 1, 1, cell.size.z - 2, map.level, House.fence_material, House.fence_data)
        # build wall corners
        if cell.neighbours[Map.z_plus] is not None:
            p = point3d(p2.x - 2, p1.y + 1, p2.z - 1)
            fill(p, 1, 1, 1, map.level, House.fence_material, House.fence_data)
        if cell.neighbours[Map.z_minus] is not None:
            p = point3d(p2.x - 2, p1.y + 1, p1.z)
            fill(p, 1, 1, 1, map.level, House.fence_material, House.fence_data)

    if cell.neighbours[Map.z_minus] is None:
        p = point3d(p1.x + 1, p1.y + 1, p1.z + 1)
        fill(p, cell.size.x - 2, 1, 1, map.level, House.fence_material, House.fence_data)
        # build wall corners
        if cell.neighbours[Map.x_plus] is not None:
            p = point3d(p2.x - 1, p1.y + 1, p1.z + 1)
            fill(p, 1, 1, 1, map.level, House.fence_material, House.fence_data)
        if cell.neighbours[Map.x_minus] is not None:
            p = point3d(p1.x, p1.y + 1, p1.z + 1)
            fill(p, 1, 1, 1, map.level, House.fence_material, House.fence_data)

    if cell.neighbours[Map.z_plus] is None:
        p = point3d(p1.x + 1, p1.y + 1, p2.z - 2)
        fill(p, cell.size.x - 2, 1, 1, map.level, House.fence_material, House.fence_data)
        if cell.neighbours[Map.x_plus] is not None:
            p = point3d(p2.x - 1, p1.y + 1, p2.z - 2)
            fill(p, 1, 1, 1, map.level, House.fence_material, House.fence_data)
        if cell.neighbours[Map.x_minus] is not None:
            p = point3d(p1.x, p1.y + 1, p2.z - 2)
            fill(p, 1, 1, 1, map.level, House.fence_material, House.fence_data)

    if Districts.debug:
        map.level.setBlockAt       (p1.x, p1.y, p1.z, 20)
        map.level.setBlockDataAt   (p1.x, p1.y, p1.z, 0)