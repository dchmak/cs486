import Utility
import Map


def build_floor(cell, map, floor_material, floor_data):
    level = map.level

    p = Utility.point3d(cell.p1.x + 1, cell.p1.y, cell.p1.z + 1)
    dimension = Utility.point3d(cell.size.x - 2, 1, cell.size.z - 2)

    if cell.neighbours[Map.y_minus] is None:
        p = cell.p1
        dimension = Utility.point3d(cell.size.x, 1, cell.size.z)
    else:
        if cell.neighbours[Map.x_minus] is not None:
            p.x = cell.p1.x
            dimension.x += 1
        if cell.neighbours[Map.z_minus] is not None:
            p.z = cell.p1.z
            dimension.z += 1
        if cell.neighbours[Map.x_plus] is not None:
            dimension.x += 1
        if cell.neighbours[Map.z_plus] is not None:
            dimension.z += 1

    Utility.fill(p, dimension.x, dimension.y, dimension.z, level, floor_material, floor_data)
