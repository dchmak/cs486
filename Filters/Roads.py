import Map
from pymclevel import alphaMaterials, MCSchematic, MCLevel, BoundingBox
from mcplatform import *
import AStar
from Utility import point2d

def build_road(path, map, thicc):

    direction = []
    for i in range(len(path)):
        point = path[i]

        if len(path) == 1:
            direction.append(0)
        elif i == len(path) - 1: # ENDPOINT
            direction.append(direction[-1])
        else:
            next_point = path[i + 1]
            if next_point.x > point.x:
                direction.append(Map.x_plus)
            elif next_point.x < point.x:
                direction.append(Map.x_minus)
            elif next_point.z > point.z:
                direction.append(Map.z_plus)
            elif next_point.z < point.z:
                direction.append(Map.z_minus)

        current_direction = direction[i]
        set_road(point.x, point.z, current_direction, map)

        if thicc:
            #EXPAND ROAD TO NON ROUGH NEIGHBOURS
            if point.x < map.w - 1 and not map.x_roughness[point.x][point.z]:
                set_road(point.x + 1, point.z, current_direction, map)
            if point.x > 0 and not map.x_roughness[point.x - 1][point.z]:
                set_road(point.x - 1, point.z, current_direction, map)
            if point.z < map.d - 1 and not map.z_roughness[point.x][point.z]:
                set_road(point.x, point.z + 1, current_direction, map)
            if point.z > 0 and not map.z_roughness[point.x][point.z - 1]:
                set_road(point.x, point.z - 1, current_direction, map)

def set_road(x, z, direction, map):
    if map.plan[x][z] == Map.farm: # skip farms
        return

    level = map.level
    map.plan[x][z] = Map.road

    height = map.height[x][z]
    next_height = height
    prev_height = height

    block = 4
    data = 0
    height_mod = 0

    #FIND DIFFERENCE BETWEEN HEIGHT AND DIRECTIONAL NEIGHBOURS
    if direction is Map.x_plus:
        if x < map.w - 1:
            next_height = map.height[x + 1][z]
        if x > 0:
            prev_height = map.height[x - 1][z]
    elif direction is Map.x_minus:
        if x > 0:
            next_height = map.height[x - 1][z]
        if x < map.w - 1:
            prev_height = map.height[x + 1][z]
    elif direction is Map.z_plus:
        if z < map.d - 1:
            next_height = map.height[x][z + 1]
        if z > 0:
            prev_height = map.height[x][z - 1]
    elif direction is Map.z_minus:
        if z > 0:
            next_height = map.height[x][z - 1]
        if z < map.d - 1:
            prev_height = map.height[x][z + 1]

    #SET BLOCK TO CORRECT TYPE
    if next_height < height and prev_height < height: #local maximum
        block = 44
        data = 3
        height_mod = 0
    if next_height > height and prev_height > height: #local minimum
        block = 44
        data = 3
        height_mod = 1
    if next_height > height and height > prev_height: #direction stairs
        block = 67
        data = direction
        height_mod = 0
    if next_height < height and height < prev_height: #opposite direction stairs
        block = 67
        data = ~direction
        height_mod = 0
    if (next_height > height and height == prev_height) or (next_height == height and height < prev_height): #slab
        block = 44
        data = 3
        height_mod = 1
    if (next_height < height and height == prev_height) or (next_height == height and height > prev_height): #no slab
        block = 4
        data = 0
        height_mod = 0

    #CLEAR SNOW AND STUFF
    level.setBlockAt(map.origin.x + x,
                     map.origin.y + map.height[x][z] + height_mod + 1,
                     map.origin.z + z,
                     0)
    level.setBlockAt(map.origin.x + x,
                     map.origin.y + map.height[x][z] + height_mod + 2,
                     map.origin.z + z,
                     0)
    level.setBlockAt(map.origin.x + x,
                     map.origin.y + map.height[x][z] + height_mod + 3,
                     map.origin.z + z,
                     0)

    #FINALLY BUILD THE BLOCK
    level.setBlockAt(map.origin.x + x,
                     map.origin.y + map.height[x][z] + height_mod,
                     map.origin.z + z,
                     block)
    level.setBlockDataAt(map.origin.x + x,
                         map.origin.y + map.height[x][z] + height_mod,
                         map.origin.z + z,
                         data)

def add_path(point, map):
    #AStar
    path = AStar.find_road_path(point, map)
    if not path is None:
        build_road(path, map, False)

def add_road(district1, district2, map):
    #Use 2d points for AStar
    p1 = point2d(district1.origin.x, district1.origin.z)
    p2 = point2d(district2.origin.x, district2.origin.z)
    #AStar
    path = AStar.get_point_path(p1, p2, map)
    if not path is None:
        build_road(path, map, True)

def add_roads(districts, map):
    visited = set()

    for district in districts:
        visited.add(district)
        for other in district.adjacency.keys():
            if not other in visited and other in districts:
                add_road(district, other, map)