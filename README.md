# CS 486 Project - Minecraft Village Generation

The project aims to generate man-made settlements in Mojang's popular sandbox game, Minecraft using AI. The settlements must be functional, adapted to the environment, believable, and aesthetically pleasing. This problem is posed yearly in the Generative Design in Minecraft Competition (GDMC) hosted by NYU http://gendesignmc.engineering.nyu.edu/. Techniques created to solve this problem may have applications in city planning and level generation in games or simulations.

## Features:
- Partition selected region into building lots and paths
- Buildings generation with a variety of custom furnitures 

## Prerequisite
- MCEdit

## Instructions
1. download MCEdit
2. clone this repository to {MCEdit-directory}/stock-filter or download the Filter folder in this repository to {MCEdit-directory}/stock-filter
3. open MCEdit
4. select a volume
5. go to filter
6. select [Filters] District and click the "Filter" Button
